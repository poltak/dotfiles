### .
Misc user app configs.

### i3wm/
i3wm/i3status configs (less updated now that my main machine is an OS X powered Macbook).

### osx/
Mac OS X scripts for configurations, including homebrew.<br />
__Note:__ All extensively based upon scripts from Mathias Bynens' [dotfiles] (https://github.com/mathiasbynens/dotfiles)
repo (more specific sources can be found commented in the actual files).

### vim/
.vimrc and plugins.<br />
__Note:__ These are no longer being updated since having discovered [Janus] (https://github.com/carlhuda/janus).
The janus/ subdirectory contains my .vimrc.after and .gvimrc.after
dotfiles.

### zsh/
Z shell configs.<br />
__Note:__ I use [oh-my-zsh] (https://github.com/robbyrussell/oh-my-zsh)
for managing most Z shell configurations. Hence .zshrc depends on
oh-my-zsh to work as intended.
__Note:__ The files in this directory are no longer updated and have
since been merged into my forked [oh-my-zsh] (https://github.com/poltak/oh-my-zsh) 
repo.

### sublimetext/
Sublime Text 2 user and default configs.
